# Des données sur l'univers tout entier

À travers cet exercice vous allez devoir récupérer diverses données fournies par la NASA afin de répondre aux besoins énoncés.

L'ensemble des points suivants sont à réaliser dans un site sous Symfony en utilisant les langages suivants : HTML, CSS, JS, PHP.

Vous devrez faire un commit et push vos fichiers sur Git à chaque partie terminée ou plus régulièrement si vous le souhaitez.

## Indications

Vous devez utiliser les APIs de la NASA : *https://api.nasa.gov/*.
Afin d'obtenir une clé de leur API vous devez simplement renseigner votre nom, prénom et adresse mail. Cette clé est nécessaire pour accéder à leurs APIs.

Pour la partie 1 vous devrez utiliser l'API <ins>Mars Rover Photos</ins>.
Pour la partie 2 vous devrez utiliser l'API <ins>NASA Image and Video Library</ins>.

Pour l'aspect visuel des pages aucun thème n'est imposé, laissez parler votre imagination. Si le thème vous intéresse vous pouvez également aller plus loin dans l'exercice en implémentant d'autres fonctionnalités.

# 1. Des photos de la planète rouge...

Vous allez tout d'abord devoir créer un Controller **Mars** dans lequel vous allez créer une route **/mars** (vous gérerez les routes à l'aide des annotations et non du fichier routes.yaml). Cette route devra retourner "Hello Mars!".

Vous allez ensuite créer une route **/curiosity**  qui va retourner une template avec le contenu suivant :

* Vous devez faire appel à l'API de la NASA afin de récupérer, aléatoirement, une photo prise par le rover Curiosity lors de son voyage sur Mars. Dans un souci d'esthétique, vous devrez récupérer uniquement les photos de la caméra de navigation de Curiosity.

* Pour ce faire, l'utilisateur doit pouvoir choisir la date de la photo qui va être récupérée à l'aide d'un input. Une fois la photo aléatoire récupérée, vous devrez l'afficher dans la page. Si jamais aucune photo n'est trouvée pour la date, pensez à n'afficher qu'aucune photo n'a été trouvée.

# 2. Des photos d'à peu près tout...

Les photos de Curiosity sont intéressantes mais vous devez maintenant récupérer toutes sortes de photos selon la recherche de l'utilisateur. 

Pour ce faire vous allez créer un Controller **Search** dans lequel vous allez créer une route **/search** qui va retourner une template avec le contenu suivant :

* Comme pour la récupération des photos de Curiosity, vous en afficherez une aléatoire parmi les résultats de la recherche et vous l'afficherez sur la page.

* L'utilisateur doit pouvoir entrer sa recherche dans un input, il utilisera un bouton pour lancer la recherche correspondant à ce qu'il a indiqué dans l'input.

# 3. Une recherche avancée...

Vous allez maintenant améliorer la recherche de la partie <ins>2</ins> en permettant à l'utilisateur de pré-remplir le champ de recherche avec un paramètre dans l'url.

Vous allez donc créer une route **/search/{query}** qui va retourner le même contenu que la partie <ins>2</ins> mais avec le champ de recherche pré-rempli avec le paramètre **query**.

# 4. Un traitement plus poussé...

Maintenant que vous avez récupéré des photos côté front, vous allez devoir les traiter côté back afin de pouvoir les afficher dès le chargement de la page.

Vous allez créer une route **/searchback/{query}** qui va cette fois-ci faire la recherche de photos côté back (grâce au client HTTP de Symfony) pour ensuite passer la photo en question à la template. La template se contentera d'afficher l'image.

# Prolongements possibles

Si le sujet vous inspire et/ou que vous souhaitez continuer plus loin, vous pouvez mettre en place d'autres intéractions avec l'API de la NASA, libre à vous de vous amuser !
